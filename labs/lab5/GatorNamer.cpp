#include <iostream>
#include <string>
#include "Gator.cpp"

using namespace std;

int main(){
  Gator g0 = Gator("Rando", "red");
  Gator g1 = Gator("Race", "blue");
  Gator g2 = Gator("Jack");
  Gator g3 = Gator("Chompers");

  cout << "The first gator's name is " << g0.getName() << " and its color is " << g0.getColor() << endl;
  cout << "The second gator's name is " << g1.getName() << " and its color is " << g1.getColor() << endl;
  cout << "The third gator's name is " << g2.getName() << " and its color is " << g2.getColor() << endl;
  cout << "The fouth gator's name is " << g3.getName() << " and its color is " << g3.getColor() << endl;
  g1.setColor("black"); //set new color
  g3.setColor("yellow"); //set new color

  cout << g1.getName() << "'s new color after setColor is " << g1.getColor() << endl;
  cout << g3.getName() << "'s new color after setColor is " << g3.getColor() << endl;

  return 0;
}
