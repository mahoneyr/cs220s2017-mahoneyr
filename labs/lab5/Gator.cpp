#include <iostream>
#include <string>
using namespace std;


class Gator{ //class

private:
  string name;
  string color;

public:
  Gator(string n, string c){ //constructor
    name = n;
    color = c;
  };

  Gator(string n){ //constructor
    name = n;
    color = "green"; //deafuly color to green
  };

  string getName(){
    return name;
  };

  string getColor(){
    return color;
  };

  void setColor(string newColor){
    color = newColor; //set a new specficied color
  };


};
