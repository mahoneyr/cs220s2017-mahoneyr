public class Gator{
  private String name;
  private String color;

  public Gator(String n, String c){ //constructor
    name = n;
    color = c;
  }

  public Gator(String n){ //constructor
    name = n;
    color = "green"; //default to green
  }

  public String getName(){
    return name;
  }

  public String getColor(){
    return color;
  }

  public void setColor(String newColor){
    color = newColor; //set new color
  }

}//class
