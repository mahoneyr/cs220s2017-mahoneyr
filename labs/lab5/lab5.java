import java.util.Scanner;

public class lab5{

  public static void main(String args[]){
      Gator g0 = new Gator("Rando", "red");
      Gator g1 = new Gator("Race", "blue");
      Gator g2 = new Gator("Jack");
      Gator g3 = new Gator("Chompers");

      System.out.println("The first gator's name is " + g0.getName() + " and its color is " + g0.getColor());
      System.out.println("The second gator's name is " + g1.getName() + " and its color is " + g1.getColor());
      System.out.println("The third gator's name is " + g2.getName() + " and its color is " + g2.getColor());
      System.out.println("The fourth gator's name is " + g3.getName() + " and its color is " + g3.getColor());
      g2.setColor("purple"); //set new color
      g0.setColor("white"); //set new color
      System.out.println(g0.getName() + "'s new color after setColor is " + g0.getColor());
      System.out.println(g2.getName() + "'s new color after setColor is " + g2.getColor());


  }//main
}//class
