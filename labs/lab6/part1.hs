--power :: Integer -> Integer -> Integer
power n k | k < 0 = error "power: negative argument"
power n 0 = 1
power n k = n * power n (k-1)

power1 n k | k < 0 = error "power1: negative argument"
power1 n k = product (replicate k n)

power2 _ 0 = 1
power2 0 _ = 0
power2 n k | k < 0 = error "power2: negative argument"
power2 n k  = let sqr x = x * x
                  half_k = k `div` 2
                  sqrHalfPower = sqr ( power2 n half_k )
             in if even k then sqrHalfPower else n * sqrHalfPower

prop_powers :: Int -> Int -> Bool
prop_powers n k = if (power n k)  == (power1 n k) && (power n k) == (power2 n k)
                      then True
                      else False
