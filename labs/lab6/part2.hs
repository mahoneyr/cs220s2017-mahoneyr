main = do
      putStrLn "Type in a list of ints to sort"     --prompt user for a list of ints to sort through
      list <- getLine     --save the input in line
      print $ "Least to Greatest"   --indicate that the output is the list sorted from least to greatest
      print $ sortfor list          --output from the sortfor function

      print $ "Greatest to Least"   --indicate that the output is the list sorted from greatest to least
      print $ sortback list         --output from the sortback funtion

sortfor :: Ord a => [a] -> [a]      --take in a variable a and reform it into a list of a as output
sortfor []     = []
sortfor (p:xs) = (sortfor lesser) ++ [p] ++ (sortfor greater) --combine the lesser and greater lists with p in the middle into 1 list
    where
        lesser  = filter (< p) xs    --filter the input into everything less than p
        greater = filter (>= p) xs    --filter the input into everything greater than p

sortback :: Ord a => [a] -> [a]     --take in a variable a and reform it into a list of a as output
sortback []     = []
sortback (p:xs) = (sortback greater) ++ [p] ++ (sortback lesser) -- same as sortfor but with greater and less than switched around
      where
          lesser  = filter (< p) xs   --filter the input into everything less than p
          greater = filter (>= p) xs  --filter the input into everything greater than p
