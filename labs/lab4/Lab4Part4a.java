public class Lab4Part4a {
  public static void main(String[] args) {
    for (int i = 0; i <= 20; i++) {
      System.out.println("pow2("+i+") = "+pow2(i));
    }
  }

  public static int pow2(int n) {

  if(n <= 0){
    return 1;
  } else {
    return 2*pow2(n-1); //run pow2 again but multiplied by 2 and subtract 1 from n
  }

  }
}
