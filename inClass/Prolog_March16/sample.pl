%plus
plus(X,Y,Sum) :- Sum is X+Y.

%max
max(X,Y,M) :- X=<Y, M=Y.
max(X,Y,M) :- X>=Y, M=X.

%animals
big(bear).
big(elephant).
small(cat).

brown(bear).
gray(elepaht).
black(cat).

dark(Z) :- black(Z).
dark(Z) :- brown(Z).



%ancestery

parent(pam, bob).
parent(tom, bob).
parent(tom, liz).
parent(bob, ann).
parent(bob, pat).
parent(pat, jim).

female(pam).
female(liz).
female(pat).
female(ann).

male(tom).
male(bob).
male(jim).


offspring(X,Y):- parent(Y,X).

mother(X,Y):- parent(X,Y), female(X).

grandparent(X,Z):- parent(X,Y), parent(Y,Z).

sister(X,Y):- parent(Z,X), parent(Z,Y), female(X).

hastwochildren(X):- offspring(X,Y), offspring(X,Z).

even(X):- 0 is mod(X,2).

odd(X):- 1 is mod(X,2).

prime(N):-
