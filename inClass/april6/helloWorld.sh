echo 'Hello, world!'

if [ -e readme.txt ] ; then
  echo 'The file "readme.txt" exists.'
else
  echo 'The file "readme.txt" does not exist.'
fi

if [ -e config.txt ] ; then
  echo 'The file "config.txt" already exists. Comparing with default...'
  diff -u config-default.txt config.txt > config.diff.txt
  echo 'A diff has been written to "config-diff.txt".'
else
  echo 'The file "config.txt" does not exit. Copying deafult...'
  cp config-default.txt config.txt
  echo '... done'
fi

echo *.txt  
