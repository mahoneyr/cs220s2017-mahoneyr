# Taken from stackoverflow:
# http://stackoverflow.com/questions/32167386/force-garbage-collection-in-python-to-free-memory

# TO DO:
# 1) run as: python memcount.py
# 2) record three memory usage numbers
# 3) call gc.collect
# 4) run the program and record the output
# 5) call gc.collect at another place
# 6) run the program and record the output
# 7) make some observations about your experiments in the comment section of this program and upload to your repository

import multiprocessing as mp
import resource
import gc


def mem():
    print('Memory usage         : % 2.2f MB' % round(
        resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0,1)
    )

mem()
gc.collect()

def memoryhog():
    print('...creating list of dicts...')
    n = 10**5
    l = []
    for i in xrange(n):
        a = 1000000*'a'
        b = 10000000*'b'
        l.append({ 'a' : a, 'b' : b })
    gc.collect()
    mem()

proc = mp.Process(target=memoryhog)
proc.start()
proc.join()
gc.collect()

mem()
