#mcs Memory.cs -out:memory#./memory

using System;
class Memory {
/* No nested classes in C# */
public class Vector {
float[] values = new float[1000];
}
public static void Main(string[] args) {
Vector v;
/* allocate memory unceasingly */
for (;;) v = new Vector();
Environment.ExitCode = 0; // generates a warning
}
}
