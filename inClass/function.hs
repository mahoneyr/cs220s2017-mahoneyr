doubleMe x = x*2 --multiplies the argument by 2

race = "My name is Race Mahoney" --prints the string

f x y = if x > 10 --simple if statement with computation
        then x * 2
        else y * 2
