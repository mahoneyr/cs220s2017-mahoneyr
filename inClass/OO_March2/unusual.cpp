
#include <iostream>
using namespace std;

// Class A has two instance variables, a constructor, and two methods:
class Allegheny {
  private:
     int x;
     int y;

  public:
     // constructor:
     Allegheny(int x, int y) {
       this->x = x*2; this->y = y*2;
     }

     int getX() { return x;}

     int getY() { return y;}
};

// Class B has two instance variables, a constructor, and two methods:
class College {
  private:
     int p;
     int q;

  public:
     // constructor:
     College(int p, int q) {
       this->p = p*2; this->q = q*2;
     }

     int getP() { return p;}

     int getQ() { return q;}
};

// Class AB inherits the variables and methods from both A and B, and
// in addition has two of its own instance variables and methods:

class AlleghenyCollege: public Allegheny, public College { // Note syntax for subclasses
  private:
     int s;
     int t;

  public:
     // constructor. Note how we call the "superclass" constructors
     // in the header:
     AlleghenyCollege(int x, int y, int p, int q, int r, int s): Allegheny(x,y), College(p,q) {
       this->s = s*2; this->t = t*2;
     }

     int getS() { return s;}

     int getT() { return t;}
};

int main() {
   AlleghenyCollege x(1,2,3,4,5,6);
   cout << x.getX() << ' ' << x.getY() << ' ' << x.getP() << ' '
        << x.getQ() << ' ' << x.getS() << ' ' << x.getT() << endl;
}
