class Parent {
   public void method() {
      System.out.println("This is parent class method.");
   }
}

class Child extends Parent {
   public void method() {
      System.out.println("This is child class method.");
   }
}

class Poly {
   public static void main(String args[]) {

      Parent parent1 = new Parent();
      Parent parent2 = new Child();

      // This will invoke the method of the parent class
      parent1.method();
      // This will invoke the method of the child class
      parent2.method();
   }
}
